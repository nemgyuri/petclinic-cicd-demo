FROM alpine:latest
COPY ./spring-petclinic/target/spring-petclinic-latest.jar /petclinic-demo/
RUN apk update && apk upgrade --no-cache && apk add --no-cache openjdk17-jre
ENTRYPOINT ["java", "-Dspring.profiles.active=mysql", "-jar", "/petclinic-demo/spring-petclinic-latest.jar"]
